### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 06ad5000-ae6a-11eb-0756-03cdafaa424e
using Pkg

# ╔═╡ 6fc7d14b-a76c-4eac-ba1e-8e50a4a2da25
Pkg.activate("Project.tomol")

# ╔═╡ 9175c348-9c20-470f-a935-81a86ba1a7a8
Pkg.add("DataStructures")

# ╔═╡ 7bdd92bf-9096-4e16-9d54-32f1039bc12e
using PlutoUI

# ╔═╡ 12f12e2a-2cc2-4fc8-8361-ca81bc625e56
using DataStructures

# ╔═╡ a4f46662-1290-42b8-a355-e3cce87f6034
md"# PROBLEM 1"

# ╔═╡ 12094a91-92fc-473a-a019-af2946fa4a88
md"## Activating My Project Enviroment"

# ╔═╡ f4866207-fed6-4a5a-98c7-43ad2b3ad84c
import Base: *

# ╔═╡ 709f8c8f-64ab-4a69-b6e2-7ca75a99bcec
begin
	const StrNum = Union{String, Number}
	Union{Number, String}
end

# ╔═╡ 72c8037e-25a6-4ffd-80d2-d86a808554f3
Base.:*(x::StrNum, y::StrNum) = string(x, y)

# ╔═╡ 359e5830-7b97-489f-901a-665c78279769
md"### Creating Office Space"

# ╔═╡ dd16796c-90ec-4e63-ae19-d0bc79b342fc
struct Office
	configuration::Dict{Symbol,Int64} 
	position::Int64
	

	function Office(packages::Vector{Int64}, pos::Int64)
		for i in packages
			if (i < 0 || i>4)
				return error("error")
			else
				return new(Dict(:W=>packages[1],:C1=>packages[2],:C2=>packages[3],:E=>packages[4]),pos)

			end
		end
	end
end

# ╔═╡ da68c521-4b05-40bc-85ef-4b228a6d13ba
goal = Office([0,0,0,0],1)

# ╔═╡ 6bbef2dd-08b1-4838-8518-1013fe25fd5d
goal1 = Office([0,0,0,0],2)

# ╔═╡ 9e4b4ca8-9f30-4a6a-9329-e109f71fe310
goal2 = Office([0,0,0,0],3)

# ╔═╡ c450e984-94a0-4c08-9ff4-4973bf4ce70d
goal3 = Office([0,0,0,0],4)

# ╔═╡ 876f9ccf-026c-40cd-a3a0-72f286b41e44
goals = [goal, goal1, goal2, goal3]

# ╔═╡ 258071e2-43b5-4266-84a1-9fff6ac231b7
startup = Office([1,3,2,1],2)

# ╔═╡ 3aa3c0c2-83c4-490b-a1c4-8d9a709a3b70
startup.position

# ╔═╡ 2f8a1f70-b3d9-4f80-84de-8289639123cd
md"### Lets get the dict values as a vector"

# ╔═╡ 9ac2eadc-4cd0-43f5-ad58-cdec10213cc1
sym = [:W, :C1, :C2, :E]

# ╔═╡ 5f19fe6d-5c0f-4539-b3ca-d899c32ab9d8
md"##### This will convert my offices to a vector to work with"

# ╔═╡ a57c23af-eef0-4444-93e6-d893cbb75ce3
function convert_to_vec(state::Office)
	sym = [:W, :C1, :C2, :E]
	i = 1
		return [state.configuration[sym[i]],state.configuration[sym[i+1]],state.configuration[sym[i+2]],state.configuration[sym[i+3]]]
end

# ╔═╡ 6d0d982c-96ac-412f-914c-ab228185906a
convert_to_vec(goal)

# ╔═╡ 3353a299-cac9-4c0a-98a5-d9a281486651
md"##### This will just check total packages in a  All Offices"

# ╔═╡ 8a78f34d-3e05-4d5c-9746-56f40900fba1
function adding_packages(state::Office)
	packs = convert_to_vec(state)
	total = 0
	 for i in 1:4
		total += packs[i]
	 end
	
	return total
end


# ╔═╡ 1e27e01c-31a6-4b54-8a97-d177324028e6
adding_packages(startup)

# ╔═╡ 858ba7cc-6abe-46a2-8703-486c5522f0e6


# ╔═╡ c2ea6c2f-4038-4ea3-a7e7-36e908fa53a8
md"## My heuristic Function"

# ╔═╡ 09169976-709e-4341-a564-c7310f6fe684
#This is a heuristic function that makes use of vectors to calculate the abs

function abs_vOF_pckgs(state::Office, goal::Office)
	packs = convert_to_vec(state)
	packs1 =  convert_to_vec(goal)
	total = 0
	 for i in 1:4
		total += abs(packs[i] - packs1[i])
	 end
	
	return total
end

# ╔═╡ aece2c09-d4d4-43e6-b596-6c5795e3ca2e
md"## My A* Search Algorithm"

# ╔═╡ d6f04b8a-c6dc-49bb-9735-88e59a6789d7
## Running the A* search.
function astarSearch(office, start, goal)
	pr_queue = []
	push!(pr_queue, (0+abs_vOF_pckgs(start, goal),+,"",start))
	visited = Set()
	
	while length(pr_queue) !=0
		_, cost, path, current = pop!(pr_queue)
		if current == goal
			return path, push!(visited, current)
		end
		if current in visited
			continue
		end
		push!(visited, current)
	
		for (direction, neighbour) in office[current]
			push!(pr_queue, (cost + abs_vOF_pckgs(neighbour, goal),
			cost + 1, path+direction, neighbour))
		end
	end
	return "No Solution"
end

# ╔═╡ ed048947-07d4-4729-bcb3-19485da8512c
md"### Creating Action Struct"

# ╔═╡ 46876ca2-c56c-42ed-ad81-d13089098a5e
struct Action
	name::String
	cost::Int64
end	

# ╔═╡ 68216385-33d1-4fc2-84b5-179dd6c30ee4
md"#### Defining Verbal actions"

# ╔═╡ a98728a1-b1fe-4880-93d5-fcbe1630f9cd
move_east = Action("me",3)

# ╔═╡ ad5c3737-05cf-457e-82ce-d141f15229c2
move_west = Action("mw",3)

# ╔═╡ 9c75eb46-f875-45c0-86ef-151d2be8a3f6
remain = Action("re",1)

# ╔═╡ 0a9d5b8c-eed6-45ad-8008-42f918db6172
collect = Action("co",5)

# ╔═╡ 2eafd3f1-86c7-4ab3-810e-bad8640becc1
movingR = "R"

# ╔═╡ e5ea22e1-a09d-4be9-b793-d68883babc95
function  generate_neighbours(state::Office)
	state_to_vec = convert_to_vec(state)
	pos = state.position
	all_neighbours = []
	prevPos = 0
	cur_vec = deepcopy(state_to_vec)
	j =	cur_vec[pos]
	j = j - 1
	name = "n"*pos*state_to_vec[pos]
	movingR = "R"
	if j >= 0
		cur_vec[pos] = j
	end
	
	
	if cur_vec != state_to_vec
		if pos == 1	
			push!(all_neighbours, Office(cur_vec,pos+1))
			name = Office(cur_vec,pos+1)
			movingR = "R"
			println(movingR)
		
		elseif pos == 4	
			push!(all_neighbours, Office(cur_vec,pos-1))
			name = Office(cur_vec,pos-1)
			movingR = "L"
			println(movingR)
		
		elseif (pos < 4) && (movingR === "L")
			push!(all_neighbours, Office(cur_vec,pos-1))
			name = Office(cur_vec,pos-1)
			println(movingR)
		
		elseif (pos > 1) && (movingR === "R") 
			push!(all_neighbours, Office(cur_vec,pos+1))
			name = Office(cur_vec,pos+1)
			println(movingR)
		else 
			push!(all_neighbours, Office(cur_vec,pos-1))
			name = Office(cur_vec,pos-1)
			movingR = "L"
			println(movingR)			
		end
	else 
		push!(all_neighbours, Office(cur_vec,pos-1))
		name = Office(cur_vec,pos-1)
		movingR = "L"
		println(movingR)
	end
		
		
	#return all_neighbours
	return name
end

# ╔═╡ 0716dd96-caeb-4045-aa82-7fa89240b37a
n1 = generate_neighbours(startup)

# ╔═╡ 02e70de1-956b-4826-b3a0-8666cf41ca86
n1c = Office([1,2,2,1],2)

# ╔═╡ 7915783f-1802-485c-a673-f0d5ceb7f842


# ╔═╡ 06f28399-73af-463b-ab63-607ea83a279a
n2 = generate_neighbours(n1)

# ╔═╡ 41efa200-ca87-4b0c-a137-b1b19a0a2bf7
n2c = Office([1,2,1,1],3)

# ╔═╡ 65c4e33a-3746-4a9e-b2b7-39ddc0b03b42
n3 = generate_neighbours(n2)

# ╔═╡ b8c877c9-d443-4635-9e42-2089a0579704
n3c = Office([1,2,1,0],4)

# ╔═╡ 968ae8b5-58e9-47f0-a35a-8ebbf8935cbf
n4c = Office([1,2,0,0],3)

# ╔═╡ c3b890f9-6b3c-403d-8bac-4b9912566d5d
n4 = Office([1,2,0,0],2)

# ╔═╡ 8e977103-980c-4dfd-95d7-3212c6c9f653
n5 = Office([1,1,0,0],1)

# ╔═╡ cfaa4d70-ee91-44cc-b128-b5fd24c5a849
n5c = Office([1,1,0,0],2)

# ╔═╡ 4a0845d3-b880-4db6-8332-5c55ded0197f
n6 = generate_neighbours(n5)

# ╔═╡ 0728b455-acef-4c2f-b419-6e82d7b26b5a
n6c = Office([0,1,0,0],1)

# ╔═╡ 9a4177ff-1cb8-4423-a224-041681909c71
n7 = generate_neighbours(n6)

# ╔═╡ 589e52ac-9506-4f42-84d3-d89118140d4a
n7c = Office([0,0,0,0],2)

# ╔═╡ a55141fb-c639-4cd5-a3a6-1cd846eed2cb


# ╔═╡ 8a5adbe9-24ca-4987-8448-76c00f84dc8a
n0 = Office([1,2,2,1],1) 

# ╔═╡ 29e1cad7-5655-4fff-ad5a-95346447e72c
n0c = Office([0,2,2,1],1) 

# ╔═╡ 8d2f967f-9666-4275-b2c8-906981aa5764
n14 = generate_neighbours(n0)

# ╔═╡ df27229b-cf89-45b9-a0bb-5d7a7f97d3d0
n14c = Office([0,1,2,1],2) 

# ╔═╡ d7eaf8ef-ad4a-40e7-9f96-a80646cb09c2
n15 = generate_neighbours(n14)

# ╔═╡ 51d20721-7249-42b2-97ca-626e598cadd1
n15c = Office([0,1,1,1],3) 

# ╔═╡ 24fc8ebe-f2ae-46e7-b899-db8d5c45c7c9
n16 = generate_neighbours(n15)

# ╔═╡ 30b5c2cd-3f30-4737-9265-2e6edba64103
n16c = Office([0,1,1,0],4) 

# ╔═╡ bd404a96-6225-409e-8f9f-de338f0ddcc6
n17 = generate_neighbours(n16)

# ╔═╡ 3f36563f-b345-42c1-8a38-3462de486d31
n17c = Office([0,1,0,0],3) 

# ╔═╡ a14f0aa6-6e80-4852-9be9-9ba93204f562
n18 = Office([0,1,0,0],2)

# ╔═╡ 85da2a3a-2c0b-45d2-9bce-949816a8df29
n18c = Office([0,0,0,0],2) 

# ╔═╡ 5b50734f-4c68-49d9-86b1-2a691a9afcef
n19 = generate_neighbours(n18)

# ╔═╡ f53cd6ce-b8c9-4586-a877-3276e72e5c46
md"## The transition model for the Offices"

# ╔═╡ 92eca37e-7111-4622-9464-8ef9828393f0
parcel_col = Dict()

# ╔═╡ 0612f7b1-13cc-4628-8a77-1690dee7022e
begin
	push!(parcel_col, startup => [(move_east,n0),(collect,n1c),(remain,startup),(move_west,n1)])
	push!(parcel_col, n1 => [(move_east,n1c),(collect,n2c),(remain,n1),(move_west,n2)])
	push!(parcel_col, n2 => [(move_east,n2c),(collect,n3c),(remain,n2),(move_west,n3)])
	push!(parcel_col, n3 => [(move_east,n3c),(collect,n4c),(remain,n3),(move_west,n4)])
	push!(parcel_col, n4 => [(move_east,n4c),(collect,n5c),(remain,n4),(move_west,n5)])
	push!(parcel_col, n5 => [(move_east,n5c),(collect,n6c),(remain,n5),(move_west,n6)])
	push!(parcel_col, n6 => [(move_east,n6c),(collect,n7c),(remain,n6),(move_west,n7)])
	push!(parcel_col, n0 => [(move_east,n0),(collect,n0c),(remain,n0),(move_west,n1c)])
end


# ╔═╡ 45049d51-c719-4eda-a019-1c56d164d535
parcel_col

# ╔═╡ 002e2fe4-a980-4835-a2ca-3928acb0f8ce


# ╔═╡ 436a0676-be6e-43e1-80b2-bdd0d2c0f478
function create_result(trans_model, ancestors, initial_state, goal_state)
	#move backward from the goal state to the inital state
	#ends when you find the initial state
	result = []
	explorer = goal_state
	while !(explorer == initial_state)
		current_state_ancestor = ancestors[explorer]
		related_transitions = trans_model[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == explorer
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		explorer = current_state_ancestor
	end
	return result
end

# ╔═╡ 1c096268-f711-41b4-85fb-debec7926aee
md"## My Breath-First Search function"

# ╔═╡ 900d297a-9fea-4e50-b3a6-29c79cc6fec8
function bfs_search(initial_state, transition_model, goal_states)
	result = []
	all_candidates = Queue{Office}()
	explored = []
	ancestors = Dict{Office, Office}()
	first_state = true
	enqueue!(all_candidates, initial_state)
	parent = initial_state
	while true
		if isempty(all_candidates)
			return []
		else
			current_state = dequeue!(all_candidates)
			#proceed with handling the current state
			push!(explored, current_state)
			candidates = transition_model[current_state]
			for single_candidate in candidates
				if !(single_candidate[1] in explored)
					push!(ancestors, single_candidate[1] => current_state)
					if (single_candidate[1] in goal_states)
						return create_result(transition_model, ancestors,
						initial_state, single_candidate[1])
					else
						enqueue!(all_candidates, single_candidate[1])
					end
				end
			end
		end
	end
end

# ╔═╡ 5a5f6648-db9e-42bc-9056-a01c21719b32
md"## My Uniform function"

# ╔═╡ 989855b2-20c0-4c88-bcf8-a17d3b490a1c
function all_search(initial_state, transition_dict, is_goal, all_candidates, add_candidate, remove_candidate)
	explored = []
	ancestors = Dict{Office, Office}()
	the_candidates = add_candidate(all_candidates, initial_state, 0)
	parent = initial_state
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			current_state = t1
			the_candidates = t2
			#proceed with handling the current state
			push!(explored, current_state)
			candidates = transition_dict[current_state]
			for single_candidate in candidates
				if !(single_candidate[1] in explored)
					push!(ancestors, single_candidate[1] => current_state)
					if (is_goal(single_candidate[1]))
						return create_result(transition_dict, ancestors,
						initial_state, single_candidate[1])
					else
						the_candidates = add_candidate(the_candidates, single_candidate[1], single_candidate[4].cost)
					end
				end
			end
		end
	end
end

# ╔═╡ fabe52ef-d80e-4174-80d5-2494c9d40b7f
md"## My heuristic function"

# ╔═╡ 84e06503-2862-4a56-a98c-ab883cc305d9
function heuristic(cell_node, goal_node)
	return abs(cell_node[1] - goal_node[1]) + abs(cell_node[2] - goal_node[2])
end

# ╔═╡ 1d942a08-282a-4035-8f20-1dc2b86a767f
md"## My A-Start function"

# ╔═╡ b7c73544-f299-4500-a132-a607df128a6e
function astar(graph, start_node, goal_node)
	pr_queue = []
	push!(pr_queue, (0 + heuristic(start_node, goal_node), 0, "", start_node))
	visited = Set()
	
	while length(pr_queue) != 0
		_, cost, path, current = push!(pr_queue)
		
		if current == goal
			return path, push!(visited, current)
		end
		if current in visited
			continue
		end
		push!(visited, current)
		
		for (direction, neighbour) in graph[current]
			headpush!(pr_queue, (cost + heuristic(neighbour, goal), cost + 1, path*direction, neighbour))
		end
	end
	return "no such path"
end
	

# ╔═╡ e0bacb57-25b5-4717-a1fd-287536bf8dd8
function goal_test(current_state::Office)
	return ! (current_state.configuration[0,0,0,0])
end

# ╔═╡ be9aa202-08cb-4b0c-89b7-dbfacda3fd0f
function add_to_queue(queue::Queue{Office}, state::Office, cost::Int64)
	enqueue!(queue, state)
	return queue
end

# ╔═╡ 3f15c604-59be-4edb-8207-4f13bf5d324e
function add_to_stack(stack::Stack{Office}, state::Office, cost::Int64)
	push!(stack, state)
	return stack
end

# ╔═╡ 60d7fe73-f38a-4dd4-88ca-418aaa22fd3b
function remove_from_pqueue_ucs(queue::PriorityQueue{Office, Int64})
	removed = dequeue!(queue)
	return (removed, queue)
end

# ╔═╡ 98968d30-a9b5-4adf-b938-1318bd8877eb
function remove_from_queue(queue::Queue{Office})
	removed = dequeue!(queue)
	return (removed, queue)
end

# ╔═╡ 3b0395f3-419d-4c38-af50-5868d2fe52b2
function remove_from_stack(stack::Stack{Office})
	removed = pop!(stack)
	return (removed, stack)
end

# ╔═╡ 4372cb68-b33a-4bc7-ae4d-d0b9eb6ad8a1
bfs_search(startup, parcel_col, goals)

# ╔═╡ da4c64f7-d2c0-428f-b3db-98d91f4f889a
md"### Generic call uniform ⋅ cost search"

# ╔═╡ 8cb14023-b786-4b9d-bf63-60d2b09cbb7d
all_search(startup, parcel_col, goal_test, PriorityQueue{Office, Int64}
(Base.Order.Reverse), add_to_pqueue_ucs, remove_from_pqueue_ucs)

# ╔═╡ Cell order:
# ╠═a4f46662-1290-42b8-a355-e3cce87f6034
# ╠═12094a91-92fc-473a-a019-af2946fa4a88
# ╠═06ad5000-ae6a-11eb-0756-03cdafaa424e
# ╠═9175c348-9c20-470f-a935-81a86ba1a7a8
# ╠═6fc7d14b-a76c-4eac-ba1e-8e50a4a2da25
# ╠═7bdd92bf-9096-4e16-9d54-32f1039bc12e
# ╠═12f12e2a-2cc2-4fc8-8361-ca81bc625e56
# ╠═f4866207-fed6-4a5a-98c7-43ad2b3ad84c
# ╠═72c8037e-25a6-4ffd-80d2-d86a808554f3
# ╠═709f8c8f-64ab-4a69-b6e2-7ca75a99bcec
# ╠═359e5830-7b97-489f-901a-665c78279769
# ╠═dd16796c-90ec-4e63-ae19-d0bc79b342fc
# ╠═da68c521-4b05-40bc-85ef-4b228a6d13ba
# ╠═6bbef2dd-08b1-4838-8518-1013fe25fd5d
# ╠═9e4b4ca8-9f30-4a6a-9329-e109f71fe310
# ╠═c450e984-94a0-4c08-9ff4-4973bf4ce70d
# ╠═876f9ccf-026c-40cd-a3a0-72f286b41e44
# ╠═258071e2-43b5-4266-84a1-9fff6ac231b7
# ╠═3aa3c0c2-83c4-490b-a1c4-8d9a709a3b70
# ╠═2f8a1f70-b3d9-4f80-84de-8289639123cd
# ╠═9ac2eadc-4cd0-43f5-ad58-cdec10213cc1
# ╠═5f19fe6d-5c0f-4539-b3ca-d899c32ab9d8
# ╠═a57c23af-eef0-4444-93e6-d893cbb75ce3
# ╠═6d0d982c-96ac-412f-914c-ab228185906a
# ╠═3353a299-cac9-4c0a-98a5-d9a281486651
# ╠═8a78f34d-3e05-4d5c-9746-56f40900fba1
# ╠═1e27e01c-31a6-4b54-8a97-d177324028e6
# ╠═858ba7cc-6abe-46a2-8703-486c5522f0e6
# ╠═c2ea6c2f-4038-4ea3-a7e7-36e908fa53a8
# ╠═09169976-709e-4341-a564-c7310f6fe684
# ╠═aece2c09-d4d4-43e6-b596-6c5795e3ca2e
# ╠═d6f04b8a-c6dc-49bb-9735-88e59a6789d7
# ╠═ed048947-07d4-4729-bcb3-19485da8512c
# ╠═46876ca2-c56c-42ed-ad81-d13089098a5e
# ╠═68216385-33d1-4fc2-84b5-179dd6c30ee4
# ╠═a98728a1-b1fe-4880-93d5-fcbe1630f9cd
# ╠═ad5c3737-05cf-457e-82ce-d141f15229c2
# ╠═9c75eb46-f875-45c0-86ef-151d2be8a3f6
# ╠═0a9d5b8c-eed6-45ad-8008-42f918db6172
# ╠═2eafd3f1-86c7-4ab3-810e-bad8640becc1
# ╠═e5ea22e1-a09d-4be9-b793-d68883babc95
# ╠═0716dd96-caeb-4045-aa82-7fa89240b37a
# ╠═02e70de1-956b-4826-b3a0-8666cf41ca86
# ╠═7915783f-1802-485c-a673-f0d5ceb7f842
# ╠═06f28399-73af-463b-ab63-607ea83a279a
# ╠═41efa200-ca87-4b0c-a137-b1b19a0a2bf7
# ╠═65c4e33a-3746-4a9e-b2b7-39ddc0b03b42
# ╠═b8c877c9-d443-4635-9e42-2089a0579704
# ╠═968ae8b5-58e9-47f0-a35a-8ebbf8935cbf
# ╠═c3b890f9-6b3c-403d-8bac-4b9912566d5d
# ╠═8e977103-980c-4dfd-95d7-3212c6c9f653
# ╠═cfaa4d70-ee91-44cc-b128-b5fd24c5a849
# ╠═4a0845d3-b880-4db6-8332-5c55ded0197f
# ╠═0728b455-acef-4c2f-b419-6e82d7b26b5a
# ╠═9a4177ff-1cb8-4423-a224-041681909c71
# ╠═589e52ac-9506-4f42-84d3-d89118140d4a
# ╠═a55141fb-c639-4cd5-a3a6-1cd846eed2cb
# ╠═8a5adbe9-24ca-4987-8448-76c00f84dc8a
# ╠═29e1cad7-5655-4fff-ad5a-95346447e72c
# ╠═8d2f967f-9666-4275-b2c8-906981aa5764
# ╠═df27229b-cf89-45b9-a0bb-5d7a7f97d3d0
# ╠═d7eaf8ef-ad4a-40e7-9f96-a80646cb09c2
# ╠═51d20721-7249-42b2-97ca-626e598cadd1
# ╠═24fc8ebe-f2ae-46e7-b899-db8d5c45c7c9
# ╠═30b5c2cd-3f30-4737-9265-2e6edba64103
# ╠═bd404a96-6225-409e-8f9f-de338f0ddcc6
# ╠═3f36563f-b345-42c1-8a38-3462de486d31
# ╠═a14f0aa6-6e80-4852-9be9-9ba93204f562
# ╠═85da2a3a-2c0b-45d2-9bce-949816a8df29
# ╠═5b50734f-4c68-49d9-86b1-2a691a9afcef
# ╠═f53cd6ce-b8c9-4586-a877-3276e72e5c46
# ╠═92eca37e-7111-4622-9464-8ef9828393f0
# ╠═0612f7b1-13cc-4628-8a77-1690dee7022e
# ╠═45049d51-c719-4eda-a019-1c56d164d535
# ╠═002e2fe4-a980-4835-a2ca-3928acb0f8ce
# ╠═436a0676-be6e-43e1-80b2-bdd0d2c0f478
# ╠═1c096268-f711-41b4-85fb-debec7926aee
# ╠═900d297a-9fea-4e50-b3a6-29c79cc6fec8
# ╠═5a5f6648-db9e-42bc-9056-a01c21719b32
# ╠═989855b2-20c0-4c88-bcf8-a17d3b490a1c
# ╠═fabe52ef-d80e-4174-80d5-2494c9d40b7f
# ╠═84e06503-2862-4a56-a98c-ab883cc305d9
# ╠═1d942a08-282a-4035-8f20-1dc2b86a767f
# ╠═b7c73544-f299-4500-a132-a607df128a6e
# ╠═e0bacb57-25b5-4717-a1fd-287536bf8dd8
# ╠═be9aa202-08cb-4b0c-89b7-dbfacda3fd0f
# ╠═3f15c604-59be-4edb-8207-4f13bf5d324e
# ╠═60d7fe73-f38a-4dd4-88ca-418aaa22fd3b
# ╠═98968d30-a9b5-4adf-b938-1318bd8877eb
# ╠═3b0395f3-419d-4c38-af50-5868d2fe52b2
# ╠═4372cb68-b33a-4bc7-ae4d-d0b9eb6ad8a1
# ╠═da4c64f7-d2c0-428f-b3db-98d91f4f889a
# ╠═8cb14023-b786-4b9d-bf63-60d2b09cbb7d
