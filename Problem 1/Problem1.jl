
Consider an agent, Verbal, in charge of the logistics in an organisation.
Once a week, Verbal collects items from all offices and sends them to their
destination. There are four offices (located next to each other): W, C1, C2
and E (see Figure 1a).
(a) Office configuration
(b) Setup at startup
Figure 1: Offices
To complete its mission, Verbal uses four actions, defined as follows:
move east: (me), to move to the next office eastward;
move west: (mw), to move to the next office westward;
remain: (re), to remain in the same office;
collect: (co), to collect a single item from an office
