### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 89f498e1-9460-4311-ae07-67740338c0c0
using Pkg

# ╔═╡ 8c45a9aa-e7ce-4ab7-a7e7-277c70991c02
Pkg.activate("Project.toml")

# ╔═╡ 7c408d50-b40a-11eb-12f1-37c29057eaf3
md"# Assignmet Problem 3" 

# ╔═╡ 8cce674a-2640-4ff2-aaf6-983426205ef4
# Student Number 219080534

# ╔═╡ 02a6c24e-0c1d-426f-b971-1cdad49aa107
md"### Setting up environment"

# ╔═╡ 9cf72450-5ae6-474d-87b1-bc9f6c38fb7a
# Declaring my domains as a vector

# ╔═╡ d2fa1024-39d3-4514-8b2c-3e29d25319f9
Dom = [1, 2, 3, 4]

# ╔═╡ 1d0e9732-c13a-4621-88ec-c10bffa97d10
# creating a struct for the variables

# ╔═╡ e7e9c5c3-ca29-4be6-ba67-e3039b0d6792
mutable struct Variables
	name::String
	value::Union{Nothing,Int64}  # takes in nothing or an integer value
	forbidden_values::Vector{}
	domain_restriction_count::Int64
end

# ╔═╡ 06474964-144f-47a0-a7e3-b95993871f3e
struct setVaules
	vars::Vector{Variables}
	constraints::Vector{Tuple{Variables,Variables}}
end

# ╔═╡ 18c8d696-9dcd-45a8-bd62-698463446159
#This is to test the restricted values

# ╔═╡ 2be3e763-3385-4f4b-a056-396ee0951202
col = rand(setdiff(Set([1,2,3,4]), Set([4,3,2])))

# ╔═╡ 3d5d3f12-9087-4b8c-af87-57c9fac5b4af
# naive inference approach

# ╔═╡ 95a63774-8e57-4103-8b8c-b751b6f83e93
# to call the rivised function

# ╔═╡ 9ed1cc87-08bb-4ab2-affe-a7af02b8b8c5
function revise(pb::T, X_i, X_j, removals::Union{Nothing, AbstractVector}) where {T <: setVaules}
    local revised::Bool = false;
    for x in deepcopy(pb.vars[X_i])
        if (all((function(y)
                    return !pb.constraints(X_i, x, X_j, y);
                end),
                pb.vars[X_j]))
            prune(pb, X_i, x, removals);
            revised = true;
        end
    end
    return revised;
end

# ╔═╡ 3f8cffb9-8c66-488f-9d44-535b12a8a296
function AC3(pb::T; queue::Union{Nothing, AbstractVector}=nothing, removals::Union{Nothing, AbstractVector}=nothing) where {T <: setVaules}
    if (typeof(queue) <: Nothing)
        queue = collect((X_i, X_k) for X_i in pb.vars for X_k in pb.constraints[X_i]);
    end
    support_pruning(pb);
    while (length(queue) != 0)
        local X = popfirst!(queue);    #Remove the first item from queue
        local X_i = getindex(X, 1);
        local X_j = getindex(X, 2);
        if (revise(pb, X_i, X_j, removals))
            if (!haskey(pb.vars, X_i))
                return false;
            end
            for X_k in pb.vars[X_i]
                if (X_k != X_i)
                    push!(queue, (X_k, X_i));
                end
            end
        end
    end
    return true;
end		

# ╔═╡ 71777300-c125-4025-aa9a-ddb2540b1acf
# backtracing With forward propagation

# ╔═╡ 35c48427-6293-4be7-9e76-ef1fad4cad3a
function solve_csp(pb::setVaules, all_assignments)
	for cur_var in pb.vars
		if cur_var.domain_restriction_count == 4
			return []
		else
			# Backtracking
			next_val = rand(setdiff(Set([1,2,3,4]), Set(cur_var.forbidden_values)))
			#assign the value to the variable
			cur_var.value = next_val
			
			#forward check
			
			for cur_constraint in pb.constraints
				if !((cur_constraint[1] == cur_var) || (cur_constraint[2] == cur_var))
					continue
				else
					if cur_constraint[1] == cur_var
						push!(cur_constraint[2].forbidden_values,next_val)
						cur_constraint[2].domain_restriction_count += 1
						# if the count reaches four you should backtrack
						# if the domain is a singleton (count == 3) propagate
					else 
						push!(cur_constraint[1].forbidden_values, next_val)
						cur_constraint[1].domain_restriction_count += 1
						# if the count reaches four you should backtrack
						# if the domain is a singleton (count == 3) propagate
					end
				end
			end
			#  add the assignment to all_assignments
			push!(all_assignments, cur_var.name => next_val)
		end
	end
	return all_assignments
end

# ╔═╡ 80b70b1a-349a-4107-aca4-153a244d8048
x1 = Variables("X1", nothing, [], 0)

# ╔═╡ 9c9493ac-8f09-442f-99e7-dc8a3c02df89
x2 = Variables("X2", nothing, [], 0)

# ╔═╡ 202d5b7a-b7de-48cd-b0fd-f5f51a29ba7e
# this will set the value of X3 to 1

# ╔═╡ dcdac830-603d-42b2-a696-2283dde05e00
x3 = Variables("X3", nothing, [2,3,4], 0)

# ╔═╡ 09a05f36-e738-4fee-b106-68cd0b82c3ce
x4 = Variables("X4", nothing, [], 0)

# ╔═╡ 422aa707-d8a8-48e7-abc2-9a5f57c59484
x5 = Variables("X5", nothing, [], 0)

# ╔═╡ 4f604a40-7bad-4fcf-9266-78450617005f
x6 = Variables("X6", nothing, [], 0)

# ╔═╡ 3d64f775-bc64-4f9c-9f5c-553c9b4e6574
x7 = Variables("X7", nothing, [], 0)

# ╔═╡ 4388c74f-e3da-4f9e-ae6e-f89e7e2fbd09
problem = setVaules([x1, x2, x3, x4, x5, x6, x7], [(x1,x2),(x1,x3),(x1,x4),(x1,x5),(x1,x6),(x2,x5),(x3,x4),(x4,x5),(x4,x6),(x5,x6),(x6,x7)])

# ╔═╡ 6e15cc8a-b861-49a5-a196-a34b08cbf0df
solve_csp(problem, [])

# ╔═╡ 05d785d6-fc96-46ab-938b-cdfa624094ff
#AC3(problem)

# ╔═╡ 4007845c-26bd-4a64-a36f-08b15e069533


# ╔═╡ e25f4eb2-2fff-4759-ab9b-ac2a43e12f0a


# ╔═╡ 9008b617-a662-449b-9508-00860cf24461


# ╔═╡ 2cee57df-86b6-4d30-a20e-be1c48ad9c5a


# ╔═╡ b711a819-4e2b-4395-9027-a19b4008b786


# ╔═╡ Cell order:
# ╠═7c408d50-b40a-11eb-12f1-37c29057eaf3
# ╠═8cce674a-2640-4ff2-aaf6-983426205ef4
# ╠═02a6c24e-0c1d-426f-b971-1cdad49aa107
# ╠═89f498e1-9460-4311-ae07-67740338c0c0
# ╠═8c45a9aa-e7ce-4ab7-a7e7-277c70991c02
# ╠═9cf72450-5ae6-474d-87b1-bc9f6c38fb7a
# ╠═d2fa1024-39d3-4514-8b2c-3e29d25319f9
# ╠═1d0e9732-c13a-4621-88ec-c10bffa97d10
# ╠═e7e9c5c3-ca29-4be6-ba67-e3039b0d6792
# ╠═06474964-144f-47a0-a7e3-b95993871f3e
# ╠═18c8d696-9dcd-45a8-bd62-698463446159
# ╠═2be3e763-3385-4f4b-a056-396ee0951202
# ╠═3d5d3f12-9087-4b8c-af87-57c9fac5b4af
# ╠═3f8cffb9-8c66-488f-9d44-535b12a8a296
# ╠═95a63774-8e57-4103-8b8c-b751b6f83e93
# ╠═9ed1cc87-08bb-4ab2-affe-a7af02b8b8c5
# ╠═71777300-c125-4025-aa9a-ddb2540b1acf
# ╠═35c48427-6293-4be7-9e76-ef1fad4cad3a
# ╠═80b70b1a-349a-4107-aca4-153a244d8048
# ╠═9c9493ac-8f09-442f-99e7-dc8a3c02df89
# ╠═202d5b7a-b7de-48cd-b0fd-f5f51a29ba7e
# ╠═dcdac830-603d-42b2-a696-2283dde05e00
# ╠═09a05f36-e738-4fee-b106-68cd0b82c3ce
# ╠═422aa707-d8a8-48e7-abc2-9a5f57c59484
# ╠═4f604a40-7bad-4fcf-9266-78450617005f
# ╠═3d64f775-bc64-4f9c-9f5c-553c9b4e6574
# ╠═4388c74f-e3da-4f9e-ae6e-f89e7e2fbd09
# ╠═6e15cc8a-b861-49a5-a196-a34b08cbf0df
# ╠═05d785d6-fc96-46ab-938b-cdfa624094ff
# ╠═4007845c-26bd-4a64-a36f-08b15e069533
# ╠═e25f4eb2-2fff-4759-ab9b-ac2a43e12f0a
# ╠═9008b617-a662-449b-9508-00860cf24461
# ╠═2cee57df-86b6-4d30-a20e-be1c48ad9c5a
# ╠═b711a819-4e2b-4395-9027-a19b4008b786
