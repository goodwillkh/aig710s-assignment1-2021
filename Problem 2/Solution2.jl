### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 568239a4-fa6e-4225-8ba0-91c3a91786a3
using Pkg

# ╔═╡ dfa7e59d-72b4-44d3-9dd9-b701bc00920e
Pkg.activate("Project.toml")

# ╔═╡ a82b3c80-b419-11eb-014a-4d48fc9f902f
md"# Assignmet Problem 2" 

# ╔═╡ 8d38a4e7-9cf3-4ebe-95a1-290b722fc105
# Student Number 219080534

# ╔═╡ b56f31a9-4a88-4cee-891f-a7f21087685d
md"### Setting up environment"

# ╔═╡ a721f6de-323a-4d0e-9cb7-80841834d960
md"### Declaring my variables and structs"

# ╔═╡ e6c32bd6-7114-43c8-aeec-b90842b9f585
@enum PlayerAction Pmin Pmax

# ╔═╡ 3f1a171f-ea16-4eb2-94ef-a97a9fb97d41
abstract type AdversarialNode end

# ╔═╡ 25f88296-06a3-49ef-b4e2-2887ca51aef9
struct TerminalNode <: AdversarialNode
	utility::Int64
end

# ╔═╡ 406626be-bab9-403d-9d45-4393dc6462ff
struct PlayerNode <: AdversarialNode
	action::PlayerAction
	nodes::Vector{Union{TerminalNode,PlayerNode}}
end

# ╔═╡ dd6ebc26-2519-4998-b9a9-48424fe9b50d
struct Game
	initial::PlayerNode
end

# ╔═╡ 971a4fdc-bcdb-4297-bd6f-00218f497608
md"### The function to solve the game"

# ╔═╡ 739d706b-6649-48fa-b842-b5913d171ac7
md"### Evaluation function"

# ╔═╡ 92c57672-15db-43e9-8b78-9f52fce913ce
function evaluate_node(node::TerminalNode)
	return node.utility
end

# ╔═╡ 2c1407a3-9698-4c80-8cd4-6ba4f30f21ee
function evaluate_node(node::PlayerNode)
	eval_nodes = map(single_node -> evaluate_node(single_node), node.nodes)
	if node.action == Pmin
		return minimum(eval_nodes)
	else
		return maximum(eval_nodes)
	end
end

# ╔═╡ 2d2c817e-fd9b-44fa-a421-3594c8febdb0
function solve_game(game::Game)
	evaluate_node(game.initial)
end

# ╔═╡ a0ed7f8c-2f79-4a82-80f1-d0063175506b
md"## the Minimax function"

# ╔═╡ a8f615ef-28f8-40e2-8cb7-7509de938bc5
 #this takis in (a node, the depth , the score value, alpha value, beta value
function minimax(node::PlayerNode, depth::Int64, s::Int64, a::Number, b::Number)
	maximum = -999999999 	#negative infinity
	minimum = 9999999999 	#positive infinity
	
    if node === TerminalNode
        #return value of the node
		return node.utility
    end   

    if node.action == Pmin 
        bestVal = minimum 
        for each in node    # this can be for i in 1:depth
            value = minimax(node*2+each, depth+1, value, a, b)
            bestVal = max( bestVal, value) 
		end
	    
		a = max(a,bestVal)
	    
		if b <= a
			return bestVal		
		end
        

    else 
		bestVal = maximum
        for each in node    # this can be for i in 1:depth
            value = minimax(node*2+each, depth+1, value, a, b)
            bestVal = min( bestVal, value) 
		end
	    b = min(b,bestVal)
	    
		if b >= a
			return bestVal		
		end
    end
end
 

# ╔═╡ 99566aa3-3602-497a-ba4e-97d845768d65

function minimax2(game::T, evaluate_node::Function,solve_game::Function, alpha::Number, beta::Number, depth::Int64) where {T <: PlayerNode}
    if (state == depth)
        return evaluate_node(state);
    end
	
	if game.action == Pmin
    	local v::Float64 = -Inf64;
    	for each in game
        	v = max(v, minimax2(game, evaluate_node, solve_game(game), alpha, beta, depth + 1));
        if (v >= beta)
            return v;
        end
        alpha = max(alpha, v);
    	end
    	return v;
	
	else
		
		local m::Float64 = Inf64;
		for each in game
			m = min(v, minimax2(game, evaluate_node, solve_game(game), alpha, beta, depth + 1));
			if (v >= alpha)
				return v;
			end
			beta = min(alpha, v);
		end
    	return m;
	end
end


# ╔═╡ 7df44c8f-b960-4699-8278-f4a9cba14921


# ╔═╡ b42ed554-9e5c-47eb-b6aa-990a660aa495
pn1 = PlayerNode(Pmin, [TerminalNode(3), TerminalNode(5), TerminalNode(10)])

# ╔═╡ bbb3c804-0bae-4ecd-8308-90118d49bb8c
pn2 = PlayerNode(Pmin, [TerminalNode(2), TerminalNode(8), TerminalNode(19)])

# ╔═╡ 9f95dc37-ea8a-4b88-a666-b80a0ba27501
pn3 = PlayerNode(Pmin, [TerminalNode(2), TerminalNode(7), TerminalNode(3)])

# ╔═╡ 11afda82-6275-48f9-81f5-c306df094849
pn4 = PlayerNode(Pmax, [pn1, pn2, pn3])

# ╔═╡ c5eb9933-ed4f-4773-99dd-93a6f074f029
game = Game(pn4)

# ╔═╡ 83c5b9c6-3479-4937-ad64-1e8e64a6636d
sol = solve_game(game)

# ╔═╡ 8bffb383-da73-4943-8679-cc0a059a0a16
minimax(game,3,sol,4,2)

# ╔═╡ 52c00376-0ec5-44dc-9137-45585eeb2dde


# ╔═╡ Cell order:
# ╠═a82b3c80-b419-11eb-014a-4d48fc9f902f
# ╠═8d38a4e7-9cf3-4ebe-95a1-290b722fc105
# ╠═b56f31a9-4a88-4cee-891f-a7f21087685d
# ╠═568239a4-fa6e-4225-8ba0-91c3a91786a3
# ╠═dfa7e59d-72b4-44d3-9dd9-b701bc00920e
# ╠═a721f6de-323a-4d0e-9cb7-80841834d960
# ╠═e6c32bd6-7114-43c8-aeec-b90842b9f585
# ╠═3f1a171f-ea16-4eb2-94ef-a97a9fb97d41
# ╠═25f88296-06a3-49ef-b4e2-2887ca51aef9
# ╠═406626be-bab9-403d-9d45-4393dc6462ff
# ╠═dd6ebc26-2519-4998-b9a9-48424fe9b50d
# ╠═971a4fdc-bcdb-4297-bd6f-00218f497608
# ╠═2d2c817e-fd9b-44fa-a421-3594c8febdb0
# ╠═739d706b-6649-48fa-b842-b5913d171ac7
# ╠═92c57672-15db-43e9-8b78-9f52fce913ce
# ╠═2c1407a3-9698-4c80-8cd4-6ba4f30f21ee
# ╠═a0ed7f8c-2f79-4a82-80f1-d0063175506b
# ╠═a8f615ef-28f8-40e2-8cb7-7509de938bc5
# ╠═99566aa3-3602-497a-ba4e-97d845768d65
# ╠═7df44c8f-b960-4699-8278-f4a9cba14921
# ╠═b42ed554-9e5c-47eb-b6aa-990a660aa495
# ╠═bbb3c804-0bae-4ecd-8308-90118d49bb8c
# ╠═9f95dc37-ea8a-4b88-a666-b80a0ba27501
# ╠═11afda82-6275-48f9-81f5-c306df094849
# ╠═c5eb9933-ed4f-4773-99dd-93a6f074f029
# ╠═83c5b9c6-3479-4937-ad64-1e8e64a6636d
# ╠═8bffb383-da73-4943-8679-cc0a059a0a16
# ╠═52c00376-0ec5-44dc-9137-45585eeb2dde
