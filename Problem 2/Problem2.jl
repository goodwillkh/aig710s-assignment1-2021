In adversarial search, a game is a problem with an initial state, multiple
players (usually two), a utility function, a goal test and the order of execution
between players (succession function). You are tasked to write a program in
Julia that:
• captures the complete description of a game provided by a user (at
runtime);
• finds a solution for the game using the alpha - beta pruning.
